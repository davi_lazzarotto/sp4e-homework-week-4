# Homework Week 11

This is the repository for homework week 11. This README file contains explaination of the organization method of particles [Exercise1](#exercise-1) and instructions on running Paraview in [Exercises 4.6](#exercise46), and instructions on how to compile and run the code in [Compilation](#compilation).

## Exercise

### Exercise 1

The top level is the singleton instance particle factory (ParticlesFactoryInterface, which is an abstract class). The instance contains a pointer with type SystemEvolution. The object SystemEvolution contains a pointer with type System. In the object System, shared pointers with type Particle are stored in a vector container, which is decorated as a particle list. The overall structure is illustrated in [Fig.1](#figure1)

![figure1](./img/figure1.svg)

### Exercise 4.6

The dumped files of the simulation are contained in `dumped\`. The number of the files corresponds to the number of time steps of the simulation. You will have a set of "step-XXXXX.csv" file in the folder.

1. Load the pack of files altogether in Paraview.
2. In the *Properties* tab change the delimiter to be a simple space character, deselect *Have Header* and click on the *Apply* button.
3. Add a filter *Table To Points* from the filter menu. Select columns *Field 0, Field 1, Field2 * to be the X, Y and Z coordinates in the *Properties* tab. 
4. In *Layout1* window, click *RenderView1*. You should be able to see a rendered grid with range [-1,1] x [-1,1].
4. In the *Display/Coloring* tab of *Properties*, change the coloring strategy from solid color to *Field 10* attribute (the temperature value).
4. Make TableToPoints filter visible in pipeline browser.
5. Click the start button to play the simulation. 

One example of the visualization result is presented in [Fig.2](#figure2).

![figure2](./img/figure2.png)


## Compilation and running

Compile the project with `cd src && mkdir build && cd build && cmake .. && make`

The input csv file for test case from 4.2 and 4.5 can be produced with `python produce_test_case_4_2.py` and `python produce_test_case_4_5.py`, respecively. 

An example of command that can run simulation with one of these test files can be run from the folder `src/build` with `./particles 10 1 ../../test_cases/test_case_4_2.csv material_point 0.001 2`

The last argument was added and correspond to the dimension of the grid L. 

The tests `test_fft` and `test_temperature` can also be run to test different functions. 
