import os
import numpy as np

# Path to output file
output_dir = "test_cases"
output_filename = "test_case_4_5.csv"

# Parameters defining the size of the x dimension of the grid
x_min = -1
x_max = 1
x_grid_size = 512

# Parameters defining the size of the x dimension of the grid
y_min = -1
y_max = 1
y_grid_size = 512

# Constant values given to each point
mass = 1
z_pos = 0
constant_temperature = 30
# No heat flux

# Creation of the grid of points
x_grid = np.linspace(x_min, x_max, num=x_grid_size)
y_grid = np.linspace(y_min, y_max, num=y_grid_size)
grid = np.reshape(np.asarray(np.meshgrid(x_grid, y_grid)), (2, -1)).T

# Create heat source
heat_source = np.zeros(grid.shape[0])
heat_source[np.sum(grid**2, axis=1) < 1] = 1

# Create content of test_case file
output_str = "\n".join([f"{point[0]:.5f} {point[1]:.5f} {z_pos} 0 0 0 0 0 0 {mass:.2f} {constant_temperature:.2f} {point_heat:.2f}" for point, point_heat in zip(grid, heat_source)])

# Dump content to file
os.makedirs(output_dir, exist_ok=True)
with open(os.path.join(output_dir, output_filename), 'w') as f:
    f.write(output_str)

