#include "compute_temperature.hh"
#include "fft.hh"
#include "material_point.hh"
#include <cmath>

/* -------------------------------------------------------------------------- */

void ComputeTemperature::compute(System& system) {

  UInt nb_particles = system.getNbParticles();

  int side = std::sqrt(nb_particles);
  Real L = system.getL();

  const Real heat_capacity = 1;
  const Real heat_conductivity = 1;
  const Real mass = system.getParticle(0).getMass();

  
  // Set matrices for heat and temperature from particles
  Matrix<complex> temperature(side);
  Matrix<complex> heat_rate(side);
  
  for (auto&& entry : index(temperature)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto& val = std::get<2>(entry);


    Particle& particle = system.getParticle(i+j*side);
    MaterialPoint& material_particle = dynamic_cast<MaterialPoint&>(particle);
        
    val = material_particle.getTemperature();
  }

  for (auto&& entry : index(heat_rate)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto& val = std::get<2>(entry);


    Particle& particle = system.getParticle(i+j*side);
    MaterialPoint& material_particle = dynamic_cast<MaterialPoint&>(particle);
        
    val = material_particle.getHeatRate();
  }

  // Compute spatial Fourier transform of the heat source and temperature
  Matrix<complex> temperature_fourier = FFT::transform(temperature);
  Matrix<complex> heat_rate_fourier = FFT::transform(heat_rate);

  Matrix<std::complex<int>> q = FFT::computeFrequencies(side);

  // Compute temperature time derivative in the Fourier domain
  Matrix<complex> temperature_derivative_fourier(side);

  for (int i = 0; i < side; i ++){
    for (int j = 0; j < side; j++){
      Real qx_2 = std::pow(q(i,j).real(), 2);
      Real qy_2 = std::pow(q(i,j).imag(), 2);


      temperature_derivative_fourier(i, j) = (heat_rate_fourier(i, j) - std::pow(2 * M_PI / L, 2) * heat_conductivity * temperature_fourier(i, j) * (qx_2 + qy_2)) / (mass * heat_capacity);
    }
  }

  // Compute the inverse Fourier transform to obtain the temperature time derivative
  Matrix<complex> temperature_derivative = FFT::itransform(temperature_derivative_fourier);

  // Use the time derivative of the temperature to obtain the temperature at the next step
  temperature_derivative *= system.getTimestep();
  temperature += temperature_derivative;

  //Set the temperature in the system
  for (auto&& entry : index(temperature)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto& val = std::get<2>(entry);

    if (i > 0 && i << (side-1) && j > 0 && j < (side-1)){ //Condition from 4.5
      Particle& particle = system.getParticle(i+j*side);
      MaterialPoint& material_particle = dynamic_cast<MaterialPoint&>(particle);
      material_particle.setTemperature(val.real());
    }
 
  }

}

/* -------------------------------------------------------------------------- */
