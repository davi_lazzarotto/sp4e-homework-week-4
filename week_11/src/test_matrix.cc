#include "matrix.hh"
#include "my_types.hh"
#include <gtest/gtest.h>

TEST(Matrix, substraction_inplace){

    const UInt size = 2;

    Matrix<Real> a(size);
    Matrix<Real> b(size);

    for (int i = 0; i < size; i++){
        for (int j = 0; j < size; j++){
            a(i, j) = i + j;
            b(i, j) = i - j;
        }
    }

    a -= b;

    for (int i = 0; i < size; i++){
        for (int j = 0; j < size; j++){
            ASSERT_NEAR(a(i, j), 2 * j, 1.0e-10);
        }
    }

}