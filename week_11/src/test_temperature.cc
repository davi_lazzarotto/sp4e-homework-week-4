#include "compute_temperature.hh"
#include "material_point.hh"
#include "material_points_factory.hh"
#include "system.hh"
#include <gtest/gtest.h>

// Fixture class
class RandomPoints : public :: testing::Test{
    protected:
        void SetUp() override {
            Real x=0,y=0,z=0;
            MaterialPointsFactory::getInstance();
            std::vector<MaterialPoint> materialpoint;

            L = 2;
            n_points = 512*512;
            nstep = 10;
            system.setTimestep(0.001);
            system.setL(L);

            for (UInt i=0; i<n_points; ++i){
                MaterialPoint mp;
                x = (static_cast<Real>(i % 512) - 256)/256; 
                y = (static_cast<Real>(i / 512) - 256)/256;
                mp.getPosition()[0] = x;
                mp.getPosition()[1] = y;
                mp.getPosition()[2] = z;
                mp.getMass() = 1;
                materialpoint.emplace_back(mp);
            }

            for(auto & p: materialpoint){
                system.addParticle(std::make_shared<MaterialPoint>(p));
            }
        }

        System system;
        UInt L;
        UInt n_points;
        UInt nstep;
};

TEST_F(RandomPoints, homogeneous){
    // set homogeneous temperature
    Real T = 23.5;

    // Initialize temperature field and heat rate
    for(auto& p: system){
        MaterialPoint& mp = dynamic_cast<MaterialPoint&>(p);
        mp.getTemperature() = T;
        mp.getHeatRate() = 0;
    }
    // Define compute method
    auto computeT = std::make_shared<ComputeTemperature>();

    // Calculate for each particle for multiple time steps
    for(int i=0; i<nstep; ++i)
        computeT->compute(system);

    // Launch Test
    for (auto& p: system) {
        MaterialPoint& mp = dynamic_cast<MaterialPoint&>(p);
        Real x = mp.getPosition()[0];
        ASSERT_NEAR(mp.getTemperature(), T, 0.01);
  }

}

TEST_F(RandomPoints, volumetric){

    // Initialize temperature field and heat rate
    for(auto& p: system){
        MaterialPoint& mp = dynamic_cast<MaterialPoint&>(p);
        Real x = mp.getPosition()[0];
        mp.getTemperature() = std::sin(2.0*M_PI*x/L);
        mp.getHeatRate() = std::sin(2.0*M_PI*x/L)*(2.0*M_PI/L)*(2.0*M_PI/L);
    }
    // Define compute method
    auto computeT = std::make_shared<ComputeTemperature>();

    // Calculate for each particle for one time step
    for(int i=0; i<nstep; ++i)
        computeT->compute(system);

    // Launch Test
    for (auto& p: system) {
        MaterialPoint& mp = dynamic_cast<MaterialPoint&>(p);
        Real x = mp.getPosition()[0];
        ASSERT_NEAR(mp.getTemperature(), std::sin(2 * M_PI * x / L), 0.1);
    }

}

TEST_F(RandomPoints, regionVolumetric){
    for(auto& p: system){
        MaterialPoint& mp = dynamic_cast<MaterialPoint&>(p);
        Real x = mp.getPosition()[0];
        mp.getTemperature() = std::sin(2.0*M_PI*x/L);

        if(x==0.5)
            mp.getHeatRate() = 1;
        else if(x==-0.5)
            mp.getHeatRate() = -1;
        else
            mp.getHeatRate() = 0;
    }
    // Define compute method
    auto computeT = std::make_shared<ComputeTemperature>();

    // Calculate for each particle for one time step
    for(int i=0; i<nstep; ++i)
        computeT->compute(system);

    // Launch Test
    for (auto& p: system) {
        MaterialPoint& mp = dynamic_cast<MaterialPoint&>(p);
        Real x = mp.getPosition()[0];
        
        if(x < -0.5 || x==0.5){
            ASSERT_NEAR(mp.getTemperature(), -x-1, 0.1);
        }
        else if((x < 0.5||x==0.5) && x > -0.5){
            ASSERT_NEAR(mp.getTemperature(), x, 0.1);
        }
        else{
            ASSERT_NEAR(mp.getTemperature(), -x+1, 0.1);
        }
  }

}