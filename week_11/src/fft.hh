#ifndef FFT_HH
#define FFT_HH
/* ------------------------------------------------------ */
#include "matrix.hh"
#include "my_types.hh"
#include <fftw3.h>
/* ------------------------------------------------------ */

struct FFT {

  static Matrix<complex> transform(Matrix<complex>& m);
  static Matrix<complex> itransform(Matrix<complex>& m);

  static Matrix<std::complex<int>> computeFrequencies(int size);
};

/* ------------------------------------------------------ */

inline Matrix<complex> FFT::transform(Matrix<complex>& m_in) {
  
  // Create variables and allocate memory
  fftw_complex *in, *out;
  fftw_plan p;

  UInt matrix_size = m_in.size();

  in = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * matrix_size * matrix_size);
  out = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * matrix_size * matrix_size);

  // Assign values to arrays
  for (auto&& entry : index(m_in)){
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto& val = std::get<2>(entry);

    in[j*matrix_size+i][0] = val.real();
    in[j*matrix_size+i][1] = val.imag();
    
  }

  // Compute plan and execute it
  p = fftw_plan_dft_2d(matrix_size, matrix_size, in, out, FFTW_FORWARD, FFTW_ESTIMATE);
  fftw_execute(p);

  // Assign values to output Matrix
  Matrix<complex> m_out(matrix_size); 

  for (auto&& entry : index(m_out)){
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto& val = std::get<2>(entry);

    val = complex(out[j*matrix_size+i][0], out[j*matrix_size+i][1]);
  }

  // Free memory
  fftw_destroy_plan(p);
  fftw_free(in); 
  fftw_free(out);

  return m_out;

}

/* ------------------------------------------------------ */

inline Matrix<complex> FFT::itransform(Matrix<complex>& m_in) {

  // Create variables and allocate memory
  fftw_complex *in, *out;
  fftw_plan p;

  UInt matrix_size = m_in.size();

  in = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * matrix_size * matrix_size);
  out = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * matrix_size * matrix_size);

  // Assign values to arrays
  for (auto&& entry : index(m_in)){
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto& val = std::get<2>(entry);

    in[j*matrix_size+i][0] = val.real();
    in[j*matrix_size+i][1] = val.imag();
    
  }

  // Compute plan and execute it
  p = fftw_plan_dft_2d(matrix_size, matrix_size, in, out, FFTW_BACKWARD, FFTW_ESTIMATE);
  fftw_execute(p);

  // Assign values to output Matrix
  Matrix<complex> m_out(matrix_size); 
  for (auto&& entry : index(m_out)){
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto& val = std::get<2>(entry);

    val = complex(out[j*matrix_size+i][0], out[j*matrix_size+i][1]) / static_cast<Real>(matrix_size * matrix_size);
    
  }

  // Free memory
  fftw_destroy_plan(p);
  fftw_free(in); 
  fftw_free(out);

  return m_out;

}

/* ------------------------------------------------------ */

inline Matrix<std::complex<int>> FFT::computeFrequencies(int size) {

  //Create Matrix and assign frequency values
  Matrix<std::complex<int>> m_out(size);
  
  for (auto&& entry : index(m_out)){
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto& val = std::get<2>(entry);

    val.real(i < (size+1)/2 ? i : int(i) - int(size));
    val.imag(j < (size+1)/2 ? j :int(j) - int(size));
    
  }

  return m_out;

}

#endif  // FFT_HH
