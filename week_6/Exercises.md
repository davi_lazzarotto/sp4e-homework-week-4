# Exercises

This file contains answers to direct questions asked in the exercise sheet. 

## Exercise 2-1

The best strategy to divide the work is according to interface, i.e., one person works on Series related code, and the other works on Dumper related code. With coherent definition of interfaces, the two parts will not rely on each other during developemnt, and can be easily integrated into `main.cc`.

## Exercise 5.1

The global complexity of the program is $O(N^2)$, since each time dump method is called, the compute method will compute the entire series.

## Exercise 5.4

The global complexitz now is $O(N)$. The re-computation is avoided and the entire series will only be computed once.

## Exercise 5.5

To reduce the rounding errors, we need to calculate the accumulation from the smallest value, i.e., $\frac{1}{N^2}$. The complexity for the whole program is $O(N)$. According to our experiments, the rounding error can be observed with the following argument setting:

```shell
maxiter = 45000
precision = 20
./src/week6 pi 45000 screen 20 10 0 1 0 3.1415 0 1.5708
```

## Exercise 6.4

For each integral, the minimum number of iterations needed to achieve the analytic value with two decimals of precision are listed here below:

* Cubic function: The analytic value is 0.25. An approximate value of 0.254975 is achieved with 101 iterations. Using one less iteration produces the value 0.255025, which rounds up to 0.26. 
* Cosinus function: The analytic value is 0. An approximate value of -0.00499892 is achieved with 617 iterations. Using one less iteration produces the value -0.00500718, which rounds down to -0.01.
* Sinus function: The analytic value is 1. An approximate value of 1.00497 is achieved with 158 iterations. Using one less iteration produces the value 1.005, which rounds up to 1.01.

Note that the integral boundaries served as input were defined with four decimal places, i.e. pi=3.1415 and pi/2=1.5708. 
