#include "Series.hh"

using namespace std;

// ========================== Exercise 5-3 ================================ //
// The method is decleared in the base class Series.
// The recorded index and values states are preserved in members current_index and current_valuee.
void Series::recordState(UInt index, Real value){
    this->current_index = index;
    this->current_value = value;
}

// ========================== Exercise 5-3 ================================ //
// If compute(UInt N) is called multiple times, and we don't want the states to be records,
// this func should be called to clear the recorded states.
void Series::clearState(){
    this->current_index = 0;
    this->current_value = 0;
}

// ========================== Exercise 2-3 ================================ //
// ========================== Exercise 5-3 ================================ //
// Implementation of the pure virtual function compute(UInt N) defined in the base class.
// Reduce complexity by record index and value states, which can be reused the next time the function is called.
Real ComputeArithmetic::compute(UInt N){
    // Initialize the result with recorded value.
    Real ans = this->current_value;

    // Get the result by summing up from n to N, where n is the recorded index.
    for(int i=this->current_index+1; i<=N; i++){
        ans += i;
    }
    
    // Record the current index and value states.
    this->recordState(N, ans);
    return ans;
}

// ========================== Exercise 3-4 ================================ //
// There is no convergence limitation for Sum(N), therefore no overload is needed.

// ========================== Exercise 2-5 ================================ //
// ========================== Exercise 5-3 ================================ //
// ========================== Exercise 5-5 ================================ //
// Implementation of the pure virtual function compute(UInt N) defined in the base class.
// Reduce complexity by record index and value states, which can be reused the next time the function is called.
// To reduce the rounding errors over floating point operation, sum should be executed from the smallest value in a reversed order. 
Real ComputePi::compute(UInt N){
    // Initialize the result with recorded value.
    Real ans = this->current_value;

    // This variable is used to get the reverse sum from n to N, where n is the recorded index.
    Real tmp = 0;

    // Get the sum from n to N in the reverse order. 
    for(int i=this->current_index+1; i<=N; i++){
        // Variable type cast is used to get the Real type value.
        tmp += 1 / Real(i*i);
    }

    ans += tmp;
    
    // Record the current index and value states.
    this->recordState(N, ans);
    return sqrt(6*ans);
}

// ========================== Exercise 3-4 ================================ //
// The convergence limitation is Pi.
Real ComputePi::getAnalyticPrediction(){
    return M_PI;
}

// ========================== Exercise 6-1 ================================ //
RiemannIntegral::RiemannIntegral(Real a, Real b, std::function<Real(Real)> f){
    this->a = a;
    this->b = b;
    this->f = f;
}

// Computation of the Riemann Integral
Real RiemannIntegral::compute(UInt N){

    Real delta_x = (this->b - this->a) / N;
    Real x_i = 0;
    Real integral_value = 0;

    for (int i=1; i<=N; i++){
        x_i = this->a + (Real)i * delta_x;
        integral_value += this->f(x_i) * delta_x;
    }

    return integral_value;
}