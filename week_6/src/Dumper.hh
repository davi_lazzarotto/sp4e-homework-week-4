#ifndef DUMPER_HPP
#define DUMPER_HPP

#include <fstream>
#include "Series.hh"

// ========================== Exercise 3-1 ================================ //
// Declearation of the abstract class DumperSeries, serving as the interface of the family of classes inheriting from DumperSeries.
class DumperSeries{
    public:
    DumperSeries(UInt maxiter, Series& series):maxiter(maxiter), series(series) {};
    virtual ~DumperSeries() {};
    virtual void dump(ostream& os) = 0;
    void setPrecision(UInt precision);

    protected:
    Series& series;
    UInt maxiter;
    UInt precision = 10;
};

// ========================== Exercise 4-5 ================================ //
inline ostream & operator << (ostream & stream, DumperSeries & _this){
    _this.dump(stream);
    return stream;
}

// ========================== Exercise 3-2 ================================ //
// Declaration of PrintSeries inherited from DumperSeries.
class PrintSeries : public DumperSeries{
    public:
    PrintSeries(UInt frequency, UInt maxiter, Series& series);
    void dump(ostream& os = cout);

    private:
    UInt frequency;
};

// ========================== Exercise 3-6 ================================ //
// Declearation of WriteSeries inherited from DumperSeries.
class WriteSeries : public DumperSeries{
    public:
    WriteSeries(string fname, UInt maxiter, Series& series);
    void dump(ostream& os);
    void setSeparator(char separator);
    string get_filename();

    private:
    string fname;
    char separator = ' ';
    string extension = ".txt";


};

#endif