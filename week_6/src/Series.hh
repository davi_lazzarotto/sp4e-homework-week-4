#ifndef SERIES_HPP
#define SERIES_HPP

#define _USE_MATH_DEFINES
#include <iostream>
#include <cmath>
#include <algorithm>
#include <functional>

using namespace std;

// Typedef 
using UInt = unsigned int;
using Real = double;

// Global variable declearation. This varaible is used to define the folder to output file.
extern const string FILE_DIR;

// ========================== Exercise 2-2 ================================ //
// ========================== Exercise 5-2 ================================ //
// Declearation of the abstract class Series, serving as the interface of the family of classes inheriting from Series.
// Add members to Series.
class Series{
    public:
    virtual ~Series() {};
    virtual Real compute(UInt N) = 0;
    virtual Real getAnalyticPrediction() {return nan("1");};
    void recordState(UInt index, Real value);
    void clearState();
    
    protected:
    
    UInt current_index = 0;
    Real current_value = 0;
    
};

// ========================== Exercise 2-3 ================================ //
// Declearation of the inherited class ComputeArithmetic.
class ComputeArithmetic : public Series{
    public:
    Real compute(UInt N);
};

// ========================== Exercise 2-5 ================================ //
// Declearation of the inherited class ComputePi.
class ComputePi : public Series{
    public:
    Real compute(UInt N);
    Real getAnalyticPrediction();
};

// ========================== Exercise 6-1 ================================ //
class RiemannIntegral : public Series{
    public:
    RiemannIntegral(Real a, Real b, std::function<Real(Real)> f);
    Real compute(UInt N);

    Real a, b;
    std::function<Real(Real)> f;

};

#endif
