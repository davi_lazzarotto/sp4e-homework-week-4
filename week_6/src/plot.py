import matplotlib.pyplot as plt
import argparse
import numpy as np

def plot_writeseries(fname: str, separator: str):
    # Read data from file and convert to numpy array
    with open(fname) as fp:
        file_content = fp.read()
    y = np.asarray(file_content.split(separator), dtype=float)
    
    # Plot the figure
    plt.plot(y,'-o', markeredgecolor='k')
    plt.grid()
    plt.xlabel("Step")
    plt.ylabel("Value")
    plt.title("Series")
    plt.savefig("series.pdf")

# Exercise 3-8
if __name__ == "__main__":
    # Arguements for reading file
    parser = argparse.ArgumentParser()
    parser.add_argument('input_filepath', type=str, help="The input file path for plotting the numerical results.")
    parser.add_argument('--separator', type=str, help="Separator used in the input file", default=' ')
    args = parser.parse_args()

    plot_writeseries(args.input_filepath, args.separator)