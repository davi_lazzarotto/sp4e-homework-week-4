#include <iomanip>
#include "Dumper.hh"

// Exercise 3-2 self-defined constructor
PrintSeries::PrintSeries(UInt frequency, UInt maxiter, Series& series):DumperSeries(maxiter, series){
    this->frequency = frequency;
}

// ========================== Exercise 4-3 ================================ //
// Implement the method setPrecision to set the output precision.
void DumperSeries::setPrecision(UInt precision){
    this->precision = precision;
}


// ========================== Exercise 3-2 ================================ //
// ========================== Exercise 3-5 ================================ //
// ========================== Exercise 4-2 ================================ //
// ========================== Exercise 4-4 ================================ //
// Implement the method dump to output to screen according to frequency and maxiter.
// Use the getAnalyticPrediction method to get the convergence limitation.
// change the arguement of dump method and provide a default parameter std::cout.
// Use the setPrecision method to get the required precision.
void PrintSeries::dump(ostream& os){
    // Before calculation, clear the recorded states from the previous computation.
    this->series.clearState();

    cout << "Printing series values to screen..." << endl;

    // When lower than maxiter, for every 'frequency' times call compute() method.
    for(int i=1; i<=this->maxiter; i+=this->frequency){
        os << setprecision(this->precision) << this->series.compute(i) << endl;
    }

    // Call analytic prediction method when available.
    if(!isnan(this->series.getAnalyticPrediction())){
        // Use system method setprecision to set the required precision.
        os << setprecision(this->precision) << this->series.getAnalyticPrediction() << endl;
    }
}

WriteSeries::WriteSeries(string fname, UInt maxiter, Series& series):DumperSeries(maxiter, series){
    this->fname = fname;
}

// ========================== Exercise 3-6 ================================ //
// ========================== Exercise 4-1 ================================ //
// Implement dump method to output numerical results and analytical prediction to a file.
// Follow the change of dump method in base class, using the paramter os.
void WriteSeries::dump(ostream& os){
    // Before calculation, clear the recorded states from the previous computation.
    this->series.clearState();
    
    cout << "Writing series values to "  << this->get_filename() << "..." << endl;

    // For all steps lower than maxiter, call compute method.
    for(int i=1; i<=this->maxiter; i++){
        os << setprecision(this->precision) << this->series.compute(i) << this->separator;
    }

    // Call analytic prediction method when available.
    if(!isnan(this->series.getAnalyticPrediction())){
        os << setprecision(this->precision) << this->series.getAnalyticPrediction();
    }
}

// ========================== Exercise 4-1 ================================ //
// Decide the output file format according to the input separator. The default format is '.txt'.
void WriteSeries::setSeparator(char separator){
    switch(separator){
        case ',':
            this->extension = ".csv";
            this->separator = ',';
            break;
        case ' ':
            this->extension = ".txt";
            this->separator = ' ';
            break;
        case '|':
            this->extension = ".psv";
            this->separator = '|';
            break;
        default:
            this->extension = ".txt";
            this->separator = ' ';
            break;
    }
}

string WriteSeries::get_filename(){
    return this->fname + this->extension;

}