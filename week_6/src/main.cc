#include <iostream>
#include <string>
#include <sstream>
#include "Dumper.hh"

using namespace std;

#define N_EXPECTED_ARGUMENTS 6

int main(int argc, char* argv[]){
    // ========================== Exercise 3-10 ================================ //
    
    
    // Use a stringstream object to get the argvs.
    stringstream sstr;
    for (int i=1; i<argc; ++i){
        sstr << argv[i] << " ";
    }
    
    cout << "Series input arguments: " << endl;
    // Get the object type that should be instantiated.
    string series_type;
    sstr >> series_type;
    cout << "   series_type: " << series_type << endl;
    // Get the maxiter parameter for dumper method.
    int maxiter;
    sstr >> maxiter;
    cout << "   maxiter: " << maxiter << endl;
    

    // ========================== Exercise 2-4 ================================ // 
    // The lines below are the answer to that exercise and are commented below because the same result can be achived by 
    // setting the series_type to arithmetic as the input. 

    // Create a ComputeArithmetic object, call the compute() method and output the results to screen.
    // ComputeArithmetic compute_a;
    // cout << "Exercise 2-4 result is ";
    // cout << compute_a.compute(maxiter) << endl;

    // ========================== Exercise 2-6 ================================ //  
    // Create a pointer pointing to the base class Series,
    // the type of the object instantiated will be decided at execution time.
    Series* ptr_compute_s;

    // Instantiate the object according to series type.
    if(series_type =="arithmetic"){
        ptr_compute_s = new ComputeArithmetic();
    }
    else if(series_type=="pi"){
        ptr_compute_s = new ComputePi();
    }
    else{
        cout << "Error: Unexpected series type " << series_type << endl;
        cout << "Available types: arithmetic, pi" << endl;
        return 1;
    }
    // Output the results to screen by polymorphism.
    cout << "Obtained value for " << series_type << " series with " << maxiter << " iterations: " << ptr_compute_s->compute(maxiter) << endl << endl << endl;

    // ========================== Exercise 3-3 ================================ //
    // ========================== Exercise 3-9 ================================ //
    cout << "Dump input arguments: " << endl;
    // Get the output type.
    string output_type;
    sstr >> output_type;
    cout << "   output_type: " << output_type << endl;
    
    // Output the results of dump method of PrintSeries.
    // The type of the instantiated object of this pointer will be decided at the execution time according to user input.
    DumperSeries* ptr_dump_s;
    UInt precision;
    sstr >> precision;
    cout << "   precision: " << precision << endl;
    if (precision < 0 || precision > 19){
        cout << "Error: Precision must be between 0 and 19, found value " << precision << endl;
        return 1;
    }

    // Instantiate the object according to output type.
    if(output_type=="screen"){
        // Set the frequency parameter, which is sepecifically decleared in PrintSeries.
        int frequency;
        sstr >> frequency;
        cout << "   frequency: " << frequency << endl;
        if (frequency <= 0 || frequency > maxiter){
            cout << "Error: Frequency must be greater than 0 and no more than " << maxiter << ", found value " << frequency << endl;
            return 1;
        }
        ptr_dump_s = new PrintSeries(frequency, maxiter, *ptr_compute_s);
        ptr_dump_s->setPrecision(precision);
        PrintSeries* ptr_dump_p = dynamic_cast <PrintSeries*> (ptr_dump_s);
        cout << *ptr_dump_p;
    }
    else if(output_type=="file"){
        // Get the output filename.
        string fname;
        sstr >> fname;
        cout << "   fname: " << fname << endl;

        // If separator is provided, use the arg; otherwise use default separator (that leads to output txt file).
        string separator_str;
        sstr >> separator_str;
        cout << "   separator: " << separator_str << endl;
        if (separator_str == "blank")
            separator_str = " ";

        char separator = separator_str[0];
        
        ptr_dump_s = new WriteSeries(fname, maxiter, *ptr_compute_s);
        ptr_dump_s->setPrecision(precision);
        // To use setSeparator method decleared in WriteSeries class, pointer of DumperSeries should be dynamically casted to WriteSeries.
        WriteSeries* ptr_dump_w = dynamic_cast <WriteSeries*> (ptr_dump_s);
        ptr_dump_w->setSeparator(separator);
        ofstream os(ptr_dump_w->get_filename());
        os << *ptr_dump_w;
    }
    else{
        cout << "Unexpected output type " << output_type << endl;
        return 1;
    }

    // ========================== Exercise 4-6 ================================ //
    // These lines of code are commented because the same formulation has already been used in the code above. 

    // Output dump results to a file using overloaded operator <<.
    // ofstream fout("dump.txt");
    // fout << *ptr_dump_s;
    // fout.close();

    // ========================== Exercise 6-1 ================================ //

    cout << endl << endl << "Riemann input arguments: " << endl;
    
    Real a_cubic, b_cubic;
    sstr >> a_cubic;
    sstr >> b_cubic;
    cout << "   a_cubic: " << a_cubic << endl;
    cout << "   b_cubic: " << b_cubic << endl;

    Real a_cos, b_cos;
    sstr >> a_cos;
    sstr >> b_cos;
    cout << "   a_cos: " << a_cos << endl;
    cout << "   b_cos: " << b_cos << endl;

    Real a_sin, b_sin;
    sstr >> a_sin;
    sstr >> b_sin;
    cout << "   a_sin: " << a_sin << endl;
    cout << "   b_sin: " << b_sin << endl;

    cout << "Computing Riemann integrals with " << maxiter << " iterations..." << endl;

    // Declares cubic function as lambda
    std::function<Real(Real)> lambda_cubic = [](Real x){return pow(x, 3);};
    // Compute integral and print
    RiemannIntegral cubic_integral(a_cubic, b_cubic, lambda_cubic);
    cout << "Riemann integral of x^3 from " << a_cubic << " to " << b_cubic << ": " << cubic_integral.compute(maxiter) << endl;

    // Declares cosinus function as lambda
    std::function<Real(Real)> lambda_cos = [](Real x){return cos(x);};
    // Compute integral and print
    RiemannIntegral cos_integral(a_cos, b_cos, lambda_cos);
    cout << "Riemann integral of cos(x) from " << a_cos << " to " << b_cos << ": " << cos_integral.compute(maxiter) << endl;
    
    // Declares sinus function as lambda
    std::function<Real(Real)> lambda_sin = [](Real x){return sin(x);};
    // Compute integral and print
    RiemannIntegral sin_integral(a_sin, b_sin, lambda_sin);
    cout << "Riemann integral of sin(x) from " << a_sin << " to " << b_sin << ": " << sin_integral.compute(maxiter) << endl;


    return 0;
}