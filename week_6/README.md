# Homework Week 6

This is the repository for homework week 6. This README file contains instructions on how to compile and run the code. Direct answers to question asked at the exercises can be found in `Exercises.md`. 

## Building and running the C++ code

To compile the C++ source code, please run the following command The executable programm `week6` will be put in `build/src`.

```shell
mkdir build
cd build
cmake ..
make
```

The executable `week6` will be build and saved in `build/src`. Its execution is split into three main steps:
 * Computation of a sum series. Either the arithmetic series or an approximation of pi. The number of iterations must be provided as input. 
 * Dumping the intermediate sums of a series. They can either be dump to a file or be printed in the terminal. In the latter case, the frequency at which the numbers are printed can be specified by the user. 
 * Computing the Riemann sums for three functions: x^3, cos(x), sin(x). The boundaries for the computation can be specified by the user. 

The executable can be run according to the following syntax:

```shell
cd build
./src/week6 [series_parameters] [dump_parameters] [riemann_parameters]
```

`series_parameters`: 
 * `series_type`: type of series to be computed. Can be either `arithmetic` or `pi`. If `arithmetic`, it computes the value of the arithmetic series $\sum_{i=1}^{maxiter}{i}$. If `pi`, computes the approximate value of pi according to $\sqrt{6 \sum_{i=1}^{maxiter}{1 / i^2}}$
 * `maxiter`: number of iterations used to compute the series.

`dump_parameters`: 
 * `output_type`: type of output. Can be either `file` or `screen`. If `file`, saves the intermediate sums of the series at a file. If `screen`, prints the intermediate sums of the series to the terminal. 
 * `precision`: precision used to print the intermediate sums. 
 The value of `output_type` determines the interpretation of the next parameters. 
 
 If `output_type` is `screen`:
 * `frequency`: the frequency at which the values are printed to the screen. For each `frequency` values computed, only one is printed.
 
 If `output_type` is `file`:
 * `fname`: name of the file to save the intermediate sums (without extension). 
 * `separator`: character used to separate intermediate sums in output file. Possible values are "," (comma), for which a csv file is produced, "|" (pipe), for which a psv file is produced, and "blank", for which a txt file is produced separated by white spaces.

 `riemann_parameters`:
  * `a_cubic`: lower boundary for the computation of the integral of the cubic function. 
  * `b_cubic`: upper boundary for the computation of the integral of the cubic function. 
  * `a_cos`: lower boundary for the computation of the integral of the cosinus function. 
  * `b_cos`: upper boundary for the computation of the integral of the cosinus function. 
  * `a_sin`: lower boundary for the computation of the integral of the sinus function. 
  * `b_sin`: upper boundary for the computation of the integral of the sinus function. 

Examples of usage: 

```shell
cd build
./src/week6 pi 1000 screen 5 10 0 1 0 3.1415 0 1.5708
./src/week6 arithmetic 50 file 5 arithmetic_series "," 0 1 0 3.1415 0 1.5708
```

## Running the python script

The python script `src/plot.py` can be run to plot the values of the series. For the computation of the approximation of pi, the last value of the file is the analytic prediction, and therefore corrresponds to the last point of the plot. The produced plot is saved at `series.pdf`. 

Usage of the script.  

```
usage: plot.py [-h] [--separator SEPARATOR] input_filepath

positional arguments:
  input_filepath        The input file path for plotting the numerical results.

optional arguments:
  -h, --help            show this help message and exit
  --separator SEPARATOR
                        Separator used in the input file
```

Examples of usage

```shell
cd src
python plot.py ../build/fout.txt
python plot.py ../build/fout.csv --separator ','
```