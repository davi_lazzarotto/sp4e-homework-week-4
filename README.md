# SP4E Homeworks

This GitLab repository contains the homeworks from Davi Lazzarotto and Bowen Huang to the EPFL course "Scientific programming for Engineers". Each directory contains a different homework. 

List of homeworks posted until now: 
* `week_4`: Contains the homework **GMRES** for week 4 due to the Wednesday 18th October.  
* `week_6`: Contains the homework **C++ classes** for week 6 due to the Wednesday 1st November.  
* `week_11`: Contains the homework **Heat equation solver** for week 11 due to the Wednesday 13rd December.  
* `week_14`: Contains the homework **Pybind and Trajectory Optimization** for week 14.  

Authors: 
Davi Lazzarotto (davi.nachtigalllazzarotto@epfl.ch)
Bowen Huang (bowen.huang@epfl.ch)
