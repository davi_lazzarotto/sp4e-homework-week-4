from scipy.optimize import minimize
from scipy.sparse.linalg import lgmres

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import axes3d
from matplotlib import cm


import numpy as np

from scipy.optimize._optimize import OptimizeResult
from typing import Callable, Union

def optimize(
        optimization_parameter: Union[Callable, tuple[np.ndarray, np.ndarray]], 
        initial_guess: np.ndarray, 
        method: str = "BFGS", 
        **kwargs
        ) -> Union[OptimizeResult, tuple[np.ndarray, int]]:
    """ Wrapper for optimization routines defined in scipy.

    Input parameters: 
    `optimization_parameter`: variable that defines the function to be optimized. If `method` is set to "LGMRES", then it should be a tuple containing 
    the A and b numpy arrays that define the matrix equation A*x = b to be solved. Otherwise, it should be a Callable defining the function to be
    minimized. 
    `initial_guess`: numpy array that defines the initial value passed to the optimization algorithm. 
    `method`: method used for optimizatino. If set to "LGMRES", then the scipy.sparse.linalg.lgmres function is used. Otherwise, it is sent as an 
    argument to the scipy.optimize.minimize function. 

    Output: 
    `out`: output of the scipy routine used for optimization. 
  
    """

    if method == "LGMRES":
        assert isinstance(optimization_parameter, tuple)
        out = lgmres(optimization_parameter[0], optimization_parameter[1], initial_guess, **kwargs)
    else:
        assert isinstance(optimization_parameter, Callable)
        out = minimize(optimization_parameter, initial_guess, method=method, **kwargs)
    
    return out
    

def plot(function_to_plot: Callable, iterations: np.ndarray, plot_title: str, output_file: str) -> None:
    """ Plotting function of two variables as a surface along with the path taken by a minimization algorithm. 

    Input parameters: 
    `function_to_plot`: callable defining the function to be plotted
    `iterations`: numpy array containing the iterations tried by the optimizer to minimize the function.
    `plot_title`: title used for the plot.
    `output_file`: path to save the plot. 
    
    """

    # Create the grid of values passed as input to the function to plot 
    x = np.arange(-3, 3, 0.01)
    x_axis, y_axis = np.meshgrid(x, x)
    x = np.reshape(np.stack((x_axis, y_axis)), (2, -1)).T

    # Computation of the function on the grid
    S = function_to_plot(x)
    S = np.reshape(S, x_axis.shape)

    # Computation of the function on the iterations of the optimizer
    iteration_function_values = function_to_plot(iterations)

    # Create plot
    fig = plt.figure()
    ax = fig.add_subplot(projection='3d')

    # Plot the function on the grid
    ax.plot_surface(x_axis, y_axis, S, shade=False, alpha=0.5, cmap=cm.coolwarm)
    ax.contour(x_axis, y_axis, S, levels=np.arange(0, 71, 10), colors='black')

    # Plot the path of the optimizer
    ax.plot(iterations[:, 0], iterations[:, 1], iteration_function_values, '--o', color='red', markeredgecolor='k', linewidth=1)
    
    # Cosmetic changes to the plot
    ax.invert_xaxis()
    ax.invert_yaxis()
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_title(plot_title)

    # Save plot
    fig.savefig(output_file)
   
    

def quadratic_function(A: np.ndarray, b: np.ndarray, x: np.ndarray) -> np.ndarray:
    """ Quadratic function th be minimized. 

    Input parameters: 
    `A`: matrix parameter of the function.
    `b`: vector parameter of the function. 
    `x`: Input of the function.
    
    """

    # Defines the shape of the input x:
    #   If x contains only one entry, then its dimensions with value 1 are excluded (in this exercise, the resulting shape of x should be [2])
    #   If x contains multiple N entries, then one dimension is added (in this exercise, the resulting shape of x should be [N, 1, 2])
    x = np.squeeze(x)
    if len(x.shape) == 2:
        x = np.expand_dims(x, 1)
        x_T = np.transpose(x, [0, 2, 1])

        # If x contains multiple entries, b is reshaped to [2, 1]
        if len(b.shape) == 1:
            b = np.expand_dims(b, -1)
    else:
        x_T = x

    # Computes the function
    y = np.matmul(x, A)
    y = np.matmul(y, x_T) / 2
    y = y - np.matmul(x, b)
    y = y.squeeze()

    return y



if __name__ == "__main__":

    # Defines quadratic function for optimization
    A = np.asarray([[8, 1], [1, 3]])
    b = np.asarray([[2], [4]])

    function_to_optimize = lambda x: quadratic_function(A, b, x)

    # Initial value to be passed to the optimizer
    initial_guess = np.asarray([0, 0])

    # Minimizes function using BFGS. A callback is used to store the iterations in `minimize_iterations`
    minimize_iterations = []
    print("Optimizing function with BFGS...")
    bfgs_solution = optimize(function_to_optimize, initial_guess, method="BFGS", callback=minimize_iterations.append)
    print(f"Solution found with BFGS: {bfgs_solution.x}")
    minimize_iterations = np.stack([initial_guess, *minimize_iterations])

    # Minimizes function using LGMRES. A callback is used to store the iterations in `lgmres_iterations`
    lgmres_iterations = []
    print("Optimizing function with LGMRES...")
    lgmres_solution = optimize((A, b), initial_guess, "LGMRES", callback=lgmres_iterations.append)
    print(f"Solution found with LGMRES: {lgmres_solution[0]}")
    lgmres_iterations = np.stack([initial_guess, *lgmres_iterations])
    
    # Plot function S(x)
    plot(function_to_optimize, minimize_iterations, plot_title='BFGS', output_file='BFGS.pdf')
    plot(function_to_optimize, lgmres_iterations, plot_title='LGMRES', output_file='LGMRES.pdf')


