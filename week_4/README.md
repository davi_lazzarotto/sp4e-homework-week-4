# Homework Week 4

Prior to running the code, `pip install -r requirements.txt` can be run to install the required python libraries.

## Exercise 1

The file `optimizer.py` contains the solution provided for Exercise 1, i.e. the functions `optimize` and `plot` requested in 1.1 and 1.2, respectively. The user can run the example provided in the exercise sheet with `python optimizer.py`, which will minimize the function `S(x)` using both BFGS and LGMRES and plot the function together with the iterations tested by the optimizer. 

Note that if the method passed as input to `optimize` is `LGMRES`, then it will find the value for `x` that solves the equation `Ax = b`, with `A` being a square matrix and `b` being a vector. Otherwise, `optimize` will minimize the function `S(x)` passed as input. In the case of the exercise, where the function `S(x)` is quadratic, the vector `x` that minimizes `S(x)` is only equivalent to the solution of `Ax=b` if the matrix `A` is **symmetric**. 

## Exercise 2
The file `GMRES.py` contains the solution for Exercise 2, i.e. the function `GMRES` requested in 2.1 and args input requested in 2.2 and 2.3. The einsum from the Numpy module is used for all matrix and vector operations as required. Functions defined in `optimizer.py` are imported to plot the solution and compare with Scipy optimization methods.

An example of command line input is 
```
python GMRES.py --A 8 1 1 3 --b 2 4 --method "GMRES" --plot --e 1e-10 --max_iter 100
```

Detailed expalanation for args of GMRES.py are listed below:
<pre>
--A          The coefficient of the linear equation Ax = b. A should be enterred in the order of zig-zag from top left to bottom right.
--b          The coefficient of the linear equation Ax = b. B should be enterred from top to bottom.
--method     The minimization method used for solving the linear equation. If the method is "GMRES", the implemented GMRES function will be used; if the method is "LGMRES", the pre-defined GMRES algorithm in scipy.sparse.linalg will be used; otherwise, the scipy.optimize.minimize methods (e.g. "BFGS" and "CG") will be used .
--plot       Plot the optimization process if defined. The figures are saved under the current path.
--e          Error limitation for the implemented GMRES algorithm.
--max_iter   Maximum number of iterations for the implemented GMRES algorithm.
</pre>
