import numpy as np
import argparse
from optimizer import optimize, quadratic_function, plot
import math

def GMRES(
        optimization_parameter: tuple[np.ndarray, np.ndarray], 
        initial_guess: np.ndarray, 
        e: np.float64,
        max_iter: np.int64
)->(np.ndarray, np.int64):
    """ Implementation of GMRES algorithm

    Input parameters:
    `optimization_parameter`: variable that defines the linear equation. It should be a tuple containing the A and b numpy arrays that define the linear equation A*x = b to be solved.
    `initial_guess`: numpy array that defines the initial value passed to the optimization algorithm.
    `e`: the error limitation. When the residual reaches the limitation, terminate the optimization and return the solution.
    `max_iter`: the maximum number of iteration in the optimization. When the maximum number of iteration is reached, terminate the optimization and return the solution.

    Output:
    `out`: output of the answer of the linear equation
    """

    # Define the coefficients of the linear equation
    #                Ax = b
    # Given an initial guess of possible solution x0
    # The initial error r is calculated as b - Ax0
    x = initial_guess
    A,b = optimization_parameter[0],optimization_parameter[1]
    r = b - np.einsum("ij,j",A,x)
    # min_r is used to record the minimum error during the optimization.
    min_r = np.linalg.norm(r)

    # Define the variables of the Arnoldi iteration
    #   q = {q1,q2,...qm} is the orthonormal vectors which form a basis for the n-th Krylov subspace.(m=max_iter)
    #   h is the upper Hessenberg matrix
    q = [np.zeros_like(r)] * max_iter
    h = np.zeros((max_iter+1, max_iter))
    q[0] = r / np.linalg.norm(r)

    minimize_iterations = []

    # Run estimation for max_iter times
    for k in range(max_iter):
        # Run Arnoldi iteration
        # Generate a new candidate vector
        q_tmp = np.einsum("ij,j",A,q[k])
        # Subtract the projections on previous vectors
        for j in range(k):
            h[j][k] = np.einsum("i,i",q[j],q_tmp)
            q_tmp = q_tmp - h[j][k]*q[j]
        # Update the Hessenberg matrix
        h[k+1][k] = np.linalg.norm(q_tmp)
        if(h[k+1][k]!=0 and k<max_iter-1):
            q[k+1] = q_tmp / h[k+1][k] 
        
        # Using least-square minimizer to find the solution y that minimize r
        b_tmp = np.zeros(max_iter+1)
        b_tmp[0] = np.linalg.norm(r)
        y = np.linalg.lstsq(h,b_tmp,rcond=None)[0]

        # Update optimized solution x according to x = Qy + x0
        x = np.einsum("ji,j",np.asarray(q),y) + initial_guess

        # Check the current error, if error reaches the limitation, break and return
        # Only new solutions that minimize min_r will be recorded during optimization
        b_result = np.einsum("ij,j",A,x)
        if(np.linalg.norm(b_result-b)<min_r):
            min_r = np.linalg.norm(b_result-b)
            minimize_iterations.append(x)
            if(min_r < e):
                break
    # Return solution for the linear equation and the iteration number
    return minimize_iterations,k


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    # Define arguments with default values
    parser.add_argument('--A', type=int, nargs='+', help='The coefficient of the linear equation Ax = b. A should be enterred in the order of zig-zag from top left to bottom right.')
    parser.add_argument('--b', type=int, nargs='+', help='The coefficient of the linear equation Ax = b. B should be enterred from top to bottom.')
    parser.add_argument('--method', type=str, nargs='?', default='GMRES', help='The minimization method used for solving the linear equation.If the method is "GMRES", the implemented GMRES function will be used; if the method is "LGMRES", the pre-defined GMRES algorithm in scipy.sparse.linalg will be used; otherwise, the scipy.optimize.minimize methods (e.g. "BFGS" and "CG") will be used.')
    parser.add_argument('--plot', action='store_true', help='Plot the optimization process if defined. The figures are saved under the current path.')
    parser.add_argument('--e', type=float, nargs='?',default=1e-8, help='Error limitation for the implemented GMRES algorithm.')
    parser.add_argument('--max_iter', type=int, nargs='?', default=1000, help='Maximum number of iterations for the implemented GMRES algorithm.')
    args = parser.parse_args()

    # Initialize input parameters of the GMRES function from pre-defined arguments
    # Infer the size of A from the input list length, and reshape A to correct matrix shape
    n = int(math.sqrt(len(args.A)))
    assert (n==math.sqrt(len(args.A))), "the input A should be a square matrix"
    A = np.asarray(args.A).reshape((n,n))
    b = np.asarray(args.b)

    #initial guess of the solution is set to all zeros with the same shape as b to adapt to linear equation with various sizes
    initial_guess = np.zeros_like(b)
    method = args.method
    is_plot = args.plot
    e = args.e 
    max_iter = args.max_iter

    # Run the optimization function and get the solution
    # Define list that stores moves for plotting the figure
    minimize_iterations = []
    function_to_optimize = lambda x: quadratic_function(A,b,x)
    if(method=='GMRES'):
        minimize_iterations,step = GMRES((A,b),initial_guess,e,max_iter)
    elif(method=='LGMRES'):
        optimize((A,b), initial_guess, method, callback=minimize_iterations.append)
    else:
        optimize(function_to_optimize, initial_guess, method, callback=minimize_iterations.append)
    
    # Plot the figure is is_plot==True
    if(is_plot):
        minimize_iterations = np.stack([initial_guess, *minimize_iterations])
        plot(function_to_optimize, minimize_iterations, plot_title=method, output_file=method+'.pdf')

    # Print the final solution
    print("The solution to the linear equation is: ",minimize_iterations[-1])