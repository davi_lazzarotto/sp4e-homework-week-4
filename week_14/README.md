# Homework Week 14

This is the repository for homework week 14. Instructions on how to compile the C++ library with python bindings, as well as on how to run the simulation and test its results using python scripts are included here. Answers to questions contained in the homework follow. 

## Compilation and running

 * Run `git submodule update --init` to initialize submodules

 * Build the C++ library with python bindings using the commands `mkdir build && cd build && cmake .. && make` from the `src` folder

 * Copy the produced shared library (file starting by `pypart` and finishing by `.so`) to the `src` directory so that it can be accessed by the python script `main.py`

 * Create a directory named `dumps` and run the simulation with the command below, which sets the time step to 1 day and the span of the simulation to 365 days:

 ```
 python main.py 365 1 init.csv planet 1
 ```

 * Run the python script `python test_trajectories.py` to validate the simulation against "measures" starting at the first of January 2000 (located in the `trajectories` folder). With the provided initialization file, the test should fail for the planet mercury and succeed for all other planets, producing the following output on the screen: "Simulated results for mercury are NOT in line with the observed data". 

 * To find the scaling factor that corrects the initial velocity of mercury, run the following command: 

 ```
 python find_correct_initial_velocity.py init.csv mercury
 ```

 * As a result, the correct scaling factor will be printed to the screen and a corresponding file with scaled initial velocities should be produced at `init_mercury_corr.csv`. Likewise, a plot with the scaling factors tested by the optimization algorithm should be produced at `optimization_mercury.pdf`

 * Re-run the simulation with the corrected csv file with the command below (this step is not actually necessary since the simulation is automaticaly run during optimization, but is kept here to make explicit that the simulation output in the `dumps` folder is updated)

 ```
 python main.py 365 1 init_mercury_corr.csv planet 1
 ```

 * Re-run the test script with `python test_trajectories.py`, which should now display the following output: "Simulated results for all planets are in line with the observed data". 


## Exercises 

### Exercise 1.2

The method `createSimulation` sets the attribute `createComputes` to a lambda function taking one `Real timestep` argument. This lambda function calls the function defined in the functor `func`, which is passed as an argument to `createSimulation`, and takes both a pointer to the current object and `timestep` as an argument. The overloaded method then calls the virtual `createSimulation`, that has an implementation only in the derived classes.

The way that `func` is invoked suggests that this is a method defined in Python, since it takes a pointer to the current object as an argument in the same way that python methods take `self` as the first argument. Therefore, this overloaded `createSimulation` method allows the python code to change the parameters of the simulation by providing a different `createComputes` method as the default defined in the C++ derived classes. 

### Exercise 2.2

There are methods that take shared pointers to objects from the `Compute` family that can be called through python bindings, such as `addCompute` from `SystemEvolution`. Default handlers cannot be used for `Compute` objects, since this would prevent the pointer of the `Compute` object defined in python to be correctly passed as an argument in the way that the C++ method expects. To solve this issue, shared pointers are defined as the handlers to all classes derived from `Compute` in `pypart.cc`. 
