#include <pybind11/pybind11.h>

namespace py = pybind11;

#include "compute_temperature.hh"
#include "compute_gravity.hh"
#include "compute_verlet_integration.hh"
#include "csv_writer.hh"
#include "material_points_factory.hh"
#include "ping_pong_balls_factory.hh"
#include "planets_factory.hh"

PYBIND11_MODULE(pypart, m) {

  m.doc() = "pybind of the Particles project";


  py::class_<System>(
    m, "System");

  py::class_<SystemEvolution>(
    m, "SystemEvolution")
    .def("evolve", &SystemEvolution::evolve)
    .def("addCompute", &SystemEvolution::addCompute)
    .def("setNSteps", &SystemEvolution::setNSteps)
    .def("setDumpFreq", &SystemEvolution::setDumpFreq)
    .def("getSystem", &SystemEvolution::getSystem, py::return_value_policy::reference);

  py::class_<ParticlesFactoryInterface>(
    m, "ParticlesFactoryInterface")
    // .def("createSimulation", [](ParticlesFactoryInterface& self, const std::string& fname, Real timestep, py::function func){self.createSimulation(fname, timestep, func);}, py::return_value_policy::reference)
    .def("createSimulation", py::overload_cast<const std::string &, Real, py::function>(&ParticlesFactoryInterface::createSimulation<py::function>), py::return_value_policy::reference) //There is a warning being thrown but it works
    .def("getInstance", &ParticlesFactoryInterface::getInstance, py::return_value_policy::reference)
    .def_property_readonly("system_evolution", &ParticlesFactoryInterface::getSystemEvolution, py::return_value_policy::reference);

  py::class_<MaterialPointsFactory, ParticlesFactoryInterface>(
    m, "MaterialPointsFactory")
    .def("createSimulation", py::overload_cast<const std::string &, Real>(&MaterialPointsFactory::createSimulation), py::return_value_policy::reference)
    .def("createSimulation", py::overload_cast<const std::string &, Real, py::function>(&ParticlesFactoryInterface::createSimulation<py::function>), py::return_value_policy::reference)
    .def("createDefaultComputes", &MaterialPointsFactory::createDefaultComputes)
    // .def("createParticle", &MaterialPointsFactory::createParticle) // Not making available in python since it is not used in main.py. In order to make it available, the class Particle should be added as well
    .def("getInstance", &MaterialPointsFactory::getInstance, py::return_value_policy::reference);

  py::class_<PlanetsFactory, ParticlesFactoryInterface>(
    m, "PlanetsFactory")
    .def("createSimulation", py::overload_cast<const std::string &, Real>(&PlanetsFactory::createSimulation), py::return_value_policy::reference)
    .def("createSimulation", py::overload_cast<const std::string &, Real, py::function>(&ParticlesFactoryInterface::createSimulation<py::function>), py::return_value_policy::reference)
    .def("createDefaultComputes", &PlanetsFactory::createDefaultComputes)
    // .def("createParticle", &PlanetsFactory::createParticle) // Not making available in python since it is not used in main.py. In order to make it available, the class Particle should be added as well
    .def("getInstance", &PlanetsFactory::getInstance, py::return_value_policy::reference);

  py::class_<PingPongBallsFactory, ParticlesFactoryInterface>(
    m, "PingPongBallsFactory")
    .def("createSimulation", py::overload_cast<const std::string &, Real>(&PingPongBallsFactory::createSimulation), py::return_value_policy::reference)
    .def("createSimulation", py::overload_cast<const std::string &, Real, py::function>(&ParticlesFactoryInterface::createSimulation<py::function>), py::return_value_policy::reference)
    // .def("createParticle", &PingPongBallsFactory::createParticle) // Not making available in python since it is not used in main.py. In order to make it available, the class Particle should be added as well
    .def("getInstance", &PingPongBallsFactory::getInstance, py::return_value_policy::reference);

  py::class_<Compute, std::shared_ptr<Compute>> compute(m, "Compute");
    // The constructor is not added since an error would be thrown due to the virtual undefine function compute in the class Compute
    //.def("compute", &Compute::compute) // This is not added because it is virtual and has no implementation

  py::class_<ComputeInteraction, std::shared_ptr<ComputeInteraction>> computeInteraction(m, "ComputeInteraction", compute);
    // The constructor is not added since an error would be thrown due to the virtual undefine function compute in the class Compute
  computeInteraction.def("applyOnPairs",  &ComputeInteraction::applyOnPairs<py::function>);

  py::class_<ComputeGravity, std::shared_ptr<ComputeGravity>> computeGravity(m, "ComputeGravity", computeInteraction);  
  computeGravity.def(py::init<>())
    .def("compute",  &ComputeGravity::compute)
    .def("setG",  &ComputeGravity::setG);

  py::class_<ComputeTemperature, std::shared_ptr<ComputeTemperature>> computeTemperature(m, "ComputeTemperature", compute);
  computeTemperature.def(py::init<>())
    .def("compute",  &ComputeTemperature::compute)
    .def_property("conductivity",  &ComputeTemperature::getConductivity, [](ComputeTemperature& self,Real conductivity){ self.getConductivity() = conductivity;})
    .def_property("capacity",  &ComputeTemperature::getCapacity, [](ComputeTemperature& self,Real capacity){ self.getCapacity() = capacity;})
    .def_property("density",  &ComputeTemperature::getDensity, [](ComputeTemperature& self,Real density){self.getDensity() = density;})
    .def_property("L",  &ComputeTemperature::getL, [](ComputeTemperature& self,Real L){self.getL() = L;})
    .def_property("deltat",  &ComputeTemperature::getDeltat, [](ComputeTemperature& self,Real deltat){self.getDeltat() = deltat;})
    .def_readwrite("implicit",  &ComputeTemperature::implicit);

  py::class_<ComputeVerletIntegration, std::shared_ptr<ComputeVerletIntegration>> computeVerletIntegration(m, "ComputeVerletIntegration", compute);
  computeVerletIntegration.def(py::init<Real>())
    .def("setDeltaT",  &ComputeVerletIntegration::setDeltaT)
    .def("compute",  &ComputeVerletIntegration::compute)
    .def("addInteraction",  &ComputeVerletIntegration::addInteraction);

  py::class_<CsvWriter, std::shared_ptr<CsvWriter>> csvWriter(m, "CsvWriter", compute);
  csvWriter.def(py::init<const std::string&>())
    .def("write",  &CsvWriter::write)
    .def("compute",  &CsvWriter::compute);

}
