import pandas as pd
import numpy as np
import math
import os
from scipy import optimize
import matplotlib.pyplot as plt

from main import main
import argparse


class Wrapper:
    def __init__(self, target_func):
        self.target_func = target_func
        self.num_calls = 0
        self.num_callbacks = 0
        self.list_call_input = []
        self.list_call_output = []
        self.list_callback_input = []
        self.list_callback_output = []
    
    def optimize(self, x, *args):
        results = self.target_func(x, *args)
        if (self.num_calls==0):
            self.list_callback_input.append(x)
            self.list_callback_output.append(results)
        self.list_call_input.append(x)
        self.list_call_output.append(results)

        self.num_calls += 1
        return results

    def callback(self, xk, *args):
        xk = np.atleast_1d(xk)

        for i, x in reversed(list(enumerate(self.list_call_input))):
            x = np.atleast_1d(x)
            if np.allclose(x, xk):
                break
        
        message = f"{xk[0]:10.5e}\t"
        message += f"{self.list_call_output[i]:10.5e}"

        self.list_callback_input.append(xk)
        self.list_callback_output.append(self.list_call_output[i])

        self.num_callbacks += 1

def readPositions(planet_name, directory):
    # get all files from the input directory
    files = os.listdir(directory)
    files.sort()

    positions = []
    for f in files:
        fpath = os.path.join(directory, f)
        df = pd.read_csv(fpath, delim_whitespace=True, header=None)
        value_by_name = df.loc[df.iloc[:,-1]==planet_name]

        # read the positions of the given planet
        x = value_by_name[0]
        y = value_by_name[1]
        z = value_by_name[2]
        positions.append((np.array([x,y,z])).astype(np.float32).transpose()[0])
    
    # return the numpy array of the trajectory
    positions = np.asarray(positions)
    return positions

def computeError(positions, positions_ref):
    # check the shape of input arrays
    # if not equal, print an error message
    num_row = positions.shape[0]
    num_row_ref = positions_ref.shape[0]
    assert num_row==num_row_ref, f"Computed trajectory length({num_row}) is different from reference trajectory length({num_row_ref})!"

    # calculate the error
    err = 0
    for i in range(num_row):
        err = err + np.matmul((positions_ref[i] - positions[i]), (positions_ref[i] - positions[i]).transpose())
    
    return math.sqrt(err)

def generateInput(scale, planet_name, input_filename, output_filename):
    # read from input csv file
    df = pd.read_csv(input_filename, delim_whitespace=True, header=None)
    
    vx = df.loc[df.iloc[:,-1]==planet_name, 3].astype(np.float32)
    vy = df.loc[df.iloc[:,-1]==planet_name, 4].astype(np.float32)
    vz = df.loc[df.iloc[:,-1]==planet_name, 5].astype(np.float32)

    # scaling velocity of a given planet
    vx *= scale
    vy *= scale
    vz *= scale

    df.loc[df.iloc[:,-1]==planet_name, 3] = vx
    df.loc[df.iloc[:,-1]==planet_name, 4] = vy
    df.loc[df.iloc[:,-1]==planet_name, 5] = vz

    # output to a new init csv file
    df.to_csv(output_filename, sep=' ', index=False, header=False)


def launchParticles(input, nb_steps, freq, timestep):
    # Run the simulation
    main(nb_steps, freq, input, "planet", timestep)

def runAndComputeError(scale, planet_name, input, nb_steps, freq, timestep):
    input_name = input.split('.')[0]
    scaled_input = input_name + "_scaled.csv"

    # generate velocity scaling
    generateInput(scale, planet_name, input, scaled_input)

    # launch particles
    launchParticles(scaled_input, nb_steps, freq, timestep)

    # read computed positions
    test_dir = "dumps"
    positions = readPositions(planet_name, test_dir)

    # read reference positions
    reference_dir = "trajectories"
    positions_ref = readPositions(planet_name, reference_dir)

    # calculate error
    err = computeError(positions, positions_ref)

    # remove file with scaled initial velocity
    os.remove(scaled_input)

    return err

def minimize_error_and_plot(nsteps, freq, filename, timestep, planet_name, target, init_scale):
    # set up the optimizer wrapper with the objective function
    target_wrap = Wrapper(target)

    # execute minimization
    opt_result = optimize.fmin(func=target_wrap.optimize, x0=init_scale, args=(planet_name, filename, nsteps, freq, timestep), ftol=1e-5, maxiter=100, callback=target_wrap.callback)
    opt_scale = opt_result[0]

    # plot error versus scaling factor
    scale_list = target_wrap.list_callback_input
    err_list = target_wrap.list_callback_output

    plt.grid()
    plt.plot(scale_list, err_list, 'o-', markeredgecolor='k')
    plt.ylabel("Error Value")
    plt.xlabel("Scaling Factor")
    plt.title("Error versus scaling factor")
    plt.savefig(f"optimization_{planet_name}.pdf")

    return opt_scale




if __name__=="__main__":
    # Find the scale that minimizes the trajectory for a planet given as input
    parser = argparse.ArgumentParser(description='Particles code')
    parser.add_argument('filename', type=str,
                        help='start/input filename for correction')
    parser.add_argument('planet_name', type=str,
                        help='name of planet to minimize the error of initial velocity')

    args = parser.parse_args()
    init_filename = args.filename
    planet_name = args.planet_name

    # Number of steps, frequency, timestep and scale used in first iteration for error minimization are set 
    nsteps = 365 # One year
    freq = 1
    timestep = 1
    init_scale = 1
    
    # Minimize error and plot values tested by minimization function
    opt_scale = minimize_error_and_plot(nsteps, freq, init_filename, timestep, planet_name, runAndComputeError, init_scale)
    print(f"Found scaling factor minimizing the error: {opt_scale}")

    # Save file with corrected initial velocities
    opt_init_filename = f"{os.path.splitext(init_filename)[0]}_{planet_name}_corr.csv"
    generateInput(opt_scale, planet_name, init_filename, opt_init_filename)

    print(f"Initial velocities with corrected scale for {planet_name} available at {opt_init_filename}")
