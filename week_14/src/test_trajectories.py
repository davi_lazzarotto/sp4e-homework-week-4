import os

def get_step_filename(folder: str, n: int):
    return os.path.join(folder, f"step-{n:04}.csv")

def get_content(file_content: str):
    # This function aranges the content of the step file into a dict

    content_dict = {}
    for line in file_content.split('\n'):
        if len(line) == 0:
            continue
        if line[0] == '#':
            continue
        line_split = line.split()
        # Each key of the dict is the planet and the value is a list containing the 3 first entries for the planet, corresponding to the position
        content_dict[line_split[-1]] = [float(line_split[i]) for i in range(3)]

    return content_dict


if __name__ == "__main__":
    # This script compares the results of the simulation to the results provided in the trajectories folder

    # Names of directories 
    reference_dir = "trajectories"
    test_dir = "dumps"
    
    # Defining function used to decide if two values are similar are not
    tolerance = 0.1
    test_similarity = lambda reference_value, test_value : abs((reference_value - test_value)/max(abs(reference_value), 1)) < tolerance

    planets_correct_result = {}

    for n_day in range(365):

        # Read csv files and organize the content of the velocity into a dict
        with open(get_step_filename(reference_dir, n_day)) as f:
            reference_step = f.read()

        with open(get_step_filename(test_dir, n_day)) as f:
            test_step = f.read()

        reference_step_dict = get_content(reference_step)
        test_step_dict = get_content(test_step)

        # For each planet, test the similarity between reference and simulated values
        for planet in reference_step_dict:

            if planet not in planets_correct_result:
                planets_correct_result[planet] = True

            if planets_correct_result[planet]:
                for reference_value, test_value in zip(reference_step_dict[planet], test_step_dict[planet]):
                    # If one of the values is not similar, set the verification to false to that planet
                    if not test_similarity(reference_value, test_value):
                        planets_correct_result[planet] = False
                        break

    test_passed = True
    for planet in planets_correct_result:
        if not planets_correct_result[planet]:
            print(f"Simulated results for {planet} are NOT in line with the observed data")
            test_passed = False
        
    if test_passed:
        print(f"Simulated results for all planets are in line with the observed data")
            
                    


        

        

    